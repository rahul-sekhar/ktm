<?php
/*
Template Name: Gallery page
*/
?>
<?php
get_header();

if (have_posts()) : while (have_posts()) : the_post();
?>

<nav id="main-nav" role="navigation">
  <?php
  $ktm_main_nav = KTM_Main_Nav::Instance();
  $ktm_main_nav->display(sanitize_title(get_the_title()));
  ?>

  <a href="#" class="pull">Menu</a>

  <div class="sub-menu">
    <div class="bar"></div>
  </div>
</nav>

<?php
$ktm_gallery = new KTM_Gallery(get_query_var('gallery-year'), get_query_var('show'), 60, get_query_var('pageno'));
get_template_part( 'partials/top-bar' );
?>

<div id="main-wrapper" class="gallery">
  <div id="filters">
    <ul class="year">
    <?php foreach($ktm_gallery->get_all_years() as $year) { ?>
      <?php if ($ktm_gallery->is_current($year)) { ?>
        <li class="current"><?php echo $year->name ?></li>
      <?php } else { ?>
        <li>
          <a href="/gallery/<?php echo $year->slug ?>"><?php echo $year->name ?></a>
        </li>
      <?php } ?>
    <?php } ?>
    </ul>

    <?php
    if ($ktm_gallery->is_valid()) {
    ?>
    <ul class="show <?php echo $ktm_gallery->get_filter_type() ?>">
      <li class="featured">
        <?php if ($ktm_gallery->get_filter_type() == 'featured') { ?>
          Featured
        <?php } else { ?>
          <a href="<?php echo $ktm_gallery->get_path(); ?>">Featured</a>
        <?php } ?>
      </li>
      <li class="all">
        <?php if ($ktm_gallery->get_filter_type() == 'all') { ?>
          All
        <?php } else { ?>
          <a href="<?php echo add_query_arg('show', 'all', $ktm_gallery->get_path()); ?>">All</a>
        <?php } ?>
      </li>
      <li class="bib-number">
        <a href="#">Find yourself</a>
        <form action="<?php echo $ktm_gallery->get_path() ?>">
          <input name="show" placeholder="Search by bib number" value="<?php echo $ktm_gallery->get_bib_number() ?>" />
          <button type="submit" class="submit">
            <span>Search</span><i class="icon-search"></i>
          </button>
        </form>
      </li>
    </ul>
    <?php
    }
    ?>
  </div>

  <div id="images">
  <?php
  $widths = array(1600, 1024, 600);
  foreach($ktm_gallery->get_images() as $image) {
  ?>
    <a class="thumb" href="<?php echo $image['url']; ?>" target="_blank"
      <?php
        foreach($widths as $width) {
          echo ' data-w' . $width . '="' . $image['sizes']['w' . $width] . '"';
        }
      ?>>
      <img src="<?php echo get_template_directory_uri(); ?>/library/images/clear.gif" data-src="<?php echo $image['sizes']['gallery-thumb'] ?>" alt="" />
      <noscript>
        <img src="<?php echo $image['sizes']['gallery-thumb'] ?>" alt="" />
      </noscript>
    </a>
  <?php
  }
  ?>
  </div>

  <div id="pagination">
  <?php
  $page_numbers = $ktm_gallery->get_pagination_numbers();
  $prev_page = null;
  if ($page_numbers) {
    if ($ktm_gallery->prev_page()) {
      echo '<a class="prev" href="' . $ktm_gallery->get_page_path($ktm_gallery->prev_page()) . '">&laquo; Prev</a>';
    }

    foreach($page_numbers as $pagenum) {
      if ($prev_page && ($pagenum - $prev_page > 1)) {
        echo '<span class="seperator">&middot;</span>';
        echo "\n";
      }
      $prev_page = $pagenum;
      if ($pagenum == $ktm_gallery->curr_page()) {
        echo '<span class="current">' . $pagenum . '</span>';
      } else {
        echo '<a href="' . $ktm_gallery->get_page_path($pagenum) . '">' . $pagenum . '</a>';
      }
      echo "\n";
    }

    if ($ktm_gallery->next_page()) {
      echo '<a class="next" href="' . $ktm_gallery->get_page_path($ktm_gallery->next_page()) . '">Next &raquo;</a>';
    }
  }
  ?>
  </div>
</div>

<?php
endwhile; endif;

get_footer();
?>
