<?php

// Fixes image fields when their image is not found
function ktm_add_empty_image_field_styles() {
  echo '<style>.acf-image-uploader .has-image { min-width: 50px; min-height: 50px; border: 1px solid #ddd; }</style>';
}
add_action( 'admin_head', 'ktm_add_empty_image_field_styles' );
