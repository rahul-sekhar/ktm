<?php

function button_shortcode($atts, $content) {
  extract( shortcode_atts( array (
    'url' => '',
    'external' => 'true',
    'disabled' => ''
  ), $atts ));

  $output = '<a class="button';

  if ($disabled) {
    $output .= ' disabled"';
  } else {
    $output .= '" href="' . htmlspecialchars($url) . '"';

    if ($external !== "false") {
      $output .= ' target="_blank"';
    }
  }



  $output .= '>' . clean_shortcode_content($content) . '</a>';
  return $output;
}
add_shortcode('button', 'button_shortcode');

function hideable_shortcode($atts, $content) {
	extract( shortcode_atts( array (
		'title' => 'Expand'
	), $atts ));

	$output = '<p class="hideable-title"><span>' . $title . '</span></p>';
	$output .= '<div class="hideable-content">' . do_shortcode(clean_shortcode_content($content)) . '</div>';
	return $output;
}
add_shortcode('hideable', 'hideable_shortcode');

function clean_shortcode_content($content) {
  $output = $content;

  // Remove starting closing p tags
	$output = preg_replace('/\A\s*<\/p>/i', '', $output);

  // Remove ending opening p tags
  $output = preg_replace('/<p>\s*\z/i', '', $output);

  // Replace starting brs with opening p tags
  $output = preg_replace('/\A\s*<br \/>/i', '<p>', $output);

	return $output;
}

function map_shortcode() {
  $map_bg = get_field('map_background', 'options');
  $map_color = get_field('map_background_color', 'options');
  $routes = get_field('routes', 'options');

  $output = '';

  $output .= '<div class="map-small">';
  $output .= '<img class="bg" style="background-color: ' . $map_color . '" src="' . $map_bg['sizes']['w600'] .'" alt="" />';

  foreach($routes as $route) {
    $output .= '<img class="route" src="' . $route['route_image']['sizes']['w600'] . '" alt="" />';
  }

  $output .= '<div class="overlay">';
  $output .= '<p>View map</p>';
  $output .= '</div>';
  $output .= '</div>';

  $output .= '<div id="map" style="background-color: ' . $map_color . '">';
  $output .= '<a href="#" class="icon-cancel close"></a>';

  ob_start();
  get_template_part('partials/map-inner');
  $ret = ob_get_contents();
  ob_end_clean();

  $output .= $ret;
  $output .= '</div>';

  return $output;
}
add_shortcode('map', 'map_shortcode');

function records_shortcode() {
  $output = '';

  $course_record_routes = get_field('course_record_routes', 'options');
  $annual_record_years = get_field('annual_record_years', 'options');

  $output = '
  <div class="records">
    <ul class="filters">
      <li class="current course"><span>Course records</span></li>';

  if($annual_record_years) {
    $output .= '
    <li class="seperator">/</li>
    <li class="year">
      <span>Winners of</span>
      <ul>';

    foreach($annual_record_years as $year) {
      $output .= '<li><span>' . $year['year'] . '</span></li>';
    }

    $output .= '
      </ul>
    </li>';
  }

  $output .= '
    </ul>

    <div class="table-container">
      <table class="course">';

  foreach(to_array($course_record_routes) as $route) {
    $output .= '
    <tr class="heading">
      <td colspan="3">
        <h4>' . $route['name'] . '</h4>
      </td>
    </tr>';

    foreach($route['events'] as $event) {
      $output .= '<tr>
        <td class="event">' . $event['name'] . '</td>
        <td class="winner">' . ucwords(strtolower($event['winner'])) . '</td>
        <td class="time">' . $event['time'] . '</td>
      </tr>';
    }
  }

  $output .= '</table>';

  foreach(to_array($annual_record_years) as $year) {
    $output .= '<table class="year-winners year' . $year['year'] . '">';

    foreach(to_array($year['routes']) as $route) {
      $output .= '
      <tr class="heading">
        <td colspan="4">
          <h4>' . $route['name'] . '</h4>
        </td>
      </tr>';

      foreach(to_array($route['events']) as $event) {
        foreach(to_array($event['winners']) as $index => $winner) {
          if ($index == 0) {
            $output .= '<tr class="event-row">
                <td class="event" rowspan="' . count($event['winners']) . '">' . $event['name'] . '</td>';
          } else {
            $output .= '<tr>';
          }

          $output .= '<td class="person">' . ($index + 1) . '. ' . ucwords(strtolower($winner['name'])) . '</td>';
          $output .= '<td class="time">' . $winner['time'] . '</td>';
          $output .= '</tr>';
        }
      }
    }

    $output .= '</table>';
  }

  $output .= '
    </div>
  </div>';

  return $output;
}
add_shortcode('records', 'records_shortcode');

?>