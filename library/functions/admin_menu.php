<?php

// Set options menu name
if( function_exists('acf_set_options_page_title') )
{
  acf_set_options_page_title( __('Other Features') );
}

add_action( 'admin_menu', 'ktm_edit_admin_menu' );
function ktm_edit_admin_menu() {
    global $menu;

    $menu[5][0] = 'Blog'; // Change Posts to Blog
    // $menu[6] = $menu[20];
    // $menu[20] = null;
    // $menu[7] = $menu[10];
    // $menu[10] = null;
}

function custom_menu_order($menu_ord) {
  if (!$menu_ord) return true;

  $new_order = array(
    'index.php', // Dashboard
    'separator1', // First separator
    'edit.php', // Posts
    'edit.php?post_type=page', // Pages
    'upload.php', // Media
    'edit.php?post_type=featured-gallery', // Featured galleries
    'acf-options-map', // Options
    'separator2', // Second separator
    'themes.php', // Appearance
    'plugins.php', // Plugins
    'users.php', // Users
    'tools.php', // Tools
    'options-general.php', // Settings
  );

  if ($menu_ord[12] == 'edit.php?post_type=acf') {
    $new_order[] = 'edit.php?post_type=acf';
  }
  $new_order[] = 'separator-last';

  return $new_order;
}
add_filter('custom_menu_order', 'custom_menu_order'); // Activate custom_menu_order
add_filter('menu_order', 'custom_menu_order');

?>