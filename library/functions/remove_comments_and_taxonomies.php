<?php
// Removes comments from admin menu
add_action( 'admin_menu', 'ktm_remove_admin_menus' );
function ktm_remove_admin_menus() {
    remove_menu_page( 'edit-comments.php' );
}
// Removes comments from post and pages
add_action('init', 'remove_comment_support', 100);

function remove_comment_support() {
    remove_post_type_support( 'post', 'comments' );
    remove_post_type_support( 'page', 'comments' );
}
// Removes comments from admin bar
function ktm_admin_bar_render() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
}
add_action( 'wp_before_admin_bar_render', 'ktm_admin_bar_render' );

// Removes the default taxonomies
function ktm_unregister_taxonomy(){
    register_taxonomy('post_tag', array());
    register_taxonomy('category', array());
}
add_action('init', 'ktm_unregister_taxonomy');
?>