<?php

// Add image size for testimonial picture
add_action( 'init', 'ktm_testimonial_image');
function ktm_testimonial_image() {
  add_image_size( 'testimonial-profile', 200, 200 );
}

?>