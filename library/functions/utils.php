<?php

// Function to return a plain text list of taxonomy terms
function ktm_get_term_list($object, $taxonomy, $seperator = ', ') {
  $terms = get_the_terms($object, $taxonomy);
  if (!$terms) {
    return '';
  }
  $terms = wp_list_pluck($terms, 'name');
  return implode($terms, $seperator);
}

function ktm_image_uri($filename) {
  return get_template_directory_uri() . '/library/images/' . $filename;
}

// Convert to an emtpy array if false
function to_array($array) {
  if (!$array) {
    return array();
  } else {
    return $array;
  }
}

?>