<?php

add_filter('mce_buttons', 'ktm_mce_buttons');
function ktm_mce_buttons( $buttons ) {
  return array(
    'bold',
    'italic',
    'formatselect',
    'bullist',
    'numlist',
    'link',
    'unlink',
    'undo',
    'redo'
  );
}

add_filter('mce_buttons_2', 'ktm_mce_buttons_2');
function ktm_mce_buttons_2( $buttons ) {
  return array();
}

add_filter('tiny_mce_before_init', 'ktm_mce_formats' );
function ktm_mce_formats($init) {
  $init['theme_advanced_blockformats'] = 'p,h4';
  return $init;
}

?>