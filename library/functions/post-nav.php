<?php

function ktm_post_nav() {
?>
  <nav class="wp-prev-next">
      <ul>
        <li class="prev-link"><?php previous_posts_link(__('&laquo; Newer entries', "ktm")) ?></li>
        <li class="next-link"><?php next_posts_link(__('Older entries &raquo;', "ktm")) ?></li>
      </ul>
    </nav>
<?php
}
?>