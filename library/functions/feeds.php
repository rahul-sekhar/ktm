<?php
require_once('lib/SimpleCache.php');
require_once('lib/TwitterAndFacebookCombinedFeed.php');

function ktm_display_feed() {
  $feedObj = new TwitterAndFacebookCombinedFeed(array(
    'twitter_name' => get_field('twitter_page_name', 'options'),
    'facebook_name' => get_field('facebook_page_name', 'options'),
    'limit' => 10
  ));

  $cache_file = get_template_directory() . '/cache/feeds.json';
  $cache_interval = 10;
  $cachedFeed = new SimpleCache(array($feedObj, 'get'), $cache_file, $cache_interval);

  $data = json_decode($cachedFeed->get());

  $output = '';
  $output .= '<ul>';
  foreach($data as $feed_item) {
    $output .= '<li class="' . $feed_item->type . '">';
    $output .= '<a href="' . htmlspecialchars($feed_item->url) . '" target="_blank">';
    $output .= wpautop(ktm_shorten_text(ktm_trim_urls(htmlspecialchars($feed_item->text)), 300));
    $output .= '</a></li>';
  }
  $output .= '</ul>';

  echo $output;
}

function ktm_shorten_text($string, $length, $more = '&hellip;') {
  if (strlen($string) <= $length) {
    return $string;
  }

  return substr($string, 0, $length) . $more;
}

function ktm_trim_urls($string) {
  $pattern = '/(https?:\/\/\S{50})\S*/i';
  $replacement = '$1&hellip;';
  return preg_replace($pattern, $replacement, $string);
}

function ktm_display_recent_posts() {
  $posts = get_posts(array(
    'posts_per_page' => 5
  ));

  $output = '';
  $output .= '<ul>';
  foreach($posts as $post) {
    $output .= '<li>';
    $output .= '<a href="' . get_permalink($post) . '">';
    $output .= '<h3>' . $post->post_title . '</h3>';
    $output .= '<p>' . wp_trim_words(strip_tags($post->post_content)) . '</p>';
    $output .= '</a></li>';
  }
  $output .= '</ul>';

  echo $output;
}


?>