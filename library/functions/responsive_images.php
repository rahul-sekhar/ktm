<?php

add_action( 'init', 'ktm_responsive_image_sizes');
function ktm_responsive_image_sizes() {
  add_image_size( 'w1600', 1600, 0 );
  add_image_size( 'w1024', 1024, 0 );
  add_image_size( 'w600', 600, 0 );

  add_image_size( 'h768', 0, 768 );

  // Add testimonials image size
  add_image_size( 'testimonial-image', 200, 200 );
}

?>
