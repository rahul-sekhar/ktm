<?php
require_once('TwitterAPIExchange.php'); // Library obtained from: https://github.com/J7mbo/twitter-api-php
require_once('FacebookGraphAPI.php');

class TwitterAndFacebookCombinedFeed {
  private $twitter_name;
  private $facebook_name;
  private $limit;

  /* The constructor takes an array of configuration values
   * twitter_name - name of the twitter user
   * facebook_name - name of the facebook page
   * limit - the maximum number of feed items to return
   *
   * If either of the names are missing, the respective feed is skipped
   */
  public function __construct(array $config) {
    if (isset($config['twitter_name'])) {
      $this->twitter_name = $config['twitter_name'];
    }

    if (isset($config['facebook_name'])) {
      $this->facebook_name = $config['facebook_name'];
    }

    if (isset($config['limit'])) {
      $this->limit = (int)$config['limit'];
    } else {
      $this->limit = 20;
    }
  }

  /* Gets a formatted twitter and facebook feed, ordered by post time, in JSON */
  public function get() {
    $feed = array();

    // Get and format the twitter feed
    if ($this->twitter_name) {
      $tweets = $this->twitter_feed($this->limit, true, true);
      $tweets = $this->format_tweets($tweets);
      $feed = array_merge($feed, $tweets);
    }

    // Get and format the facebook feed
    if ($this->facebook_name) {
      $posts = $this->facebook_feed();
      $posts = $this->format_facebook($posts);
      $feed = array_merge($feed, $posts);
    }

    // Sort and limit the feeds
    usort($feed, array($this, 'cmp_timestamp'));
    $feed = array_slice($feed, 0, $this->limit);

    return json_encode($feed);
  }

  /* Private function to obtain the twitter feed */
  private function twitter_feed($count, $include_retweets, $exclude_replies) {
    $url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
    $requestMethod = 'GET';
    $getfields = array(
      'screen_name' => $this->twitter_name,
      'count' => $count,
      'include_rts' => $include_retweets,
      'exclude_replies' => $exclude_replies,
      'trim_user' => 'true'
    );

    $settings = ktm_twitter_keys();
    $twitter = new TwitterAPIExchange($settings);
    $result = $twitter->setGetfields($getfields)
     ->buildOauth($url, $requestMethod)
     ->performRequest();

    return json_decode($result);
  }

  /* Private function to obtain the facebook feed */
  private function facebook_feed() {
    $config = ktm_facebook_keys();
    $facebook = new FacebookGraphAPI($config);
    $fields = array(
      'fields' => 'id,type,message,picture,link,created_time'
    );

    $result = $facebook->setUrl($this->facebook_name . '/posts')
      ->setFields($fields)
      ->performRequest();

    $result = json_decode($result);
    return $result ? $result->data : null;
  }


  /* Private function to format the twitter feed, keeping only necessary data */
  private function format_tweets($tweet_data) {
    $output = array();

    if ($tweet_data) {
      foreach($tweet_data as $tweet) {
        $timestamp = strtotime($tweet->created_at);
        $output[] = array(
          'text' => $tweet->text,
          'url' => 'https://twitter.com/' . $this->twitter_name,
          'timestamp' => $timestamp,
          'type' => 'twitter'
        );
      }
    }

    return $output;
  }

  /* Private function to format the facebook feed, keeping only necessary data */
  private function format_facebook($facebook_data) {
    $output = array();

    if ($facebook_data) {
      foreach($facebook_data as $post) {

        // Skip posts without a message
        if (!isset($post->message)) {
          continue;
        }

        $timestamp = strtotime($post->created_time);

        // Photos link to the photo, while other posts have a permalink
        if ($post->type == 'photo') {
          $url = $post->link;
        } else {
          $id = explode('_', $post->id);
          $id = $id[1];
          $url = 'https://facebook.com/' . $this->facebook_name . '/posts/' . $id;
        }
        $output[] = array(
          'text' => $post->message,
          'url' => $url,
          'timestamp' => $timestamp,
          'type' => 'facebook'
        );
      }
    }

    return $output;
  }

  /* Function to sort by timestamp */
  private function cmp_timestamp($a, $b) {
    return $b['timestamp'] - $a['timestamp'];
  }
}
?>