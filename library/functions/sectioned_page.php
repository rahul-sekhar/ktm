<?php
/*
Custom type for pages with sections
*/

add_action( 'init', 'ktm_add_sectioned_page');
function ktm_add_sectioned_page() {
	register_post_type( 'sectioned_page',
		array('labels' => array(
			'name' => __('Sectioned Pages', 'ktm'), /* This is the Title of the Group */
			'singular_name' => __('Sectioned Page', 'ktm'), /* This is the individual type */
			'all_items' => __('All Sectioned Pages', 'ktm'), /* the all items menu item */
			'add_new' => __('Add New', 'ktm'), /* The add new menu item */
			'add_new_item' => __('Add New Sectioned Page', 'ktm'), /* Add New Display Title */
			'edit' => __( 'Edit', 'ktm' ), /* Edit Dialog */
			'edit_item' => __('Edit Sectioned Page', 'ktm'), /* Edit Display Title */
			'new_item' => __('New Sectioned Page', 'ktm'), /* New Display Title */
			'view_item' => __('View Sectioned Page', 'ktm'), /* View Display Title */
			'search_items' => __('Search Sectioned Page', 'ktm'), /* Search Custom Type Title */
			'not_found' =>  __('No sectioned pages found', 'ktm'), /* This displays if there are no entries yet */
			'not_found_in_trash' => __('No sectioned pages found in the trash', 'ktm'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of labels */
			'description' => __( 'Pages with sections that appear as submenu items', 'ktm' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
			'has_archive' => false,
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor' ),
			'rewrite' =>  array( 'slug'=>'pages', 'with_front' => false )

	 	) /* end of options */
	); /* end of register post type */
}