<?php

/*
Notes - the shortcodes: [map] and [records] can be used to display the
interactive map and records section anywhere
*/

require_once('plugins/advanced-custom-fields/acf.php' );
require_once('plugins/acf-repeater/acf-repeater.php' );
require_once('plugins/acf-gallery/acf-gallery.php' );
require_once('plugins/acf-options-page/acf-options-page.php' );

require_once('plugins/add-from-server/add-from-server.php');
afs_load();

require_once('acf-race-day-fields.php');

acf_add_options_sub_page('Map');
acf_add_options_sub_page('Records');
acf_add_options_sub_page('Social Pages');

require_once('sensitive/api_keys.php');

require_once('functions/utils.php');
require_once('functions/nav_menu.php');
require_once('functions/shortcodes.php');
require_once('functions/image_handling.php');
require_once('functions/feeds.php');
require_once('functions/gallery-type.php');
require_once('functions/gallery-page.php');
require_once('functions/remove_comments_and_taxonomies.php');
require_once('functions/post-nav.php');
require_once('functions/testimonials.php');
require_once('functions/responsive_images.php');
require_once('functions/media-bulk-actions.php');
require_once('functions/bib-tagging.php');

// Admin customisation
require_once('functions/tinymce.php');
require_once('functions/admin_menu.php');
require_once('functions/admin-image-field-fix.php');

// Relative URLS - taken from the Roots theme
add_theme_support('root-relative-urls');
require_once('functions/relative-urls.php');

// Output page titles
function ktm_title() {
  bloginfo('name');

  if (!is_front_page()) {
    echo " | ";
    wp_title('');
  }
}


// Remove the WYSIWYG editor from pages - using custom fields instead
function ktm_remove_editor() {
  remove_post_type_support('page', 'editor');
}
add_action('admin_init', 'ktm_remove_editor');


// Remove the wordpress admin bar
add_filter('show_admin_bar', '__return_false');


// Enequeue additional scripts
function ktm_scripts() {
  global $wp_styles; // call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way
  if (!is_admin()) {

    $version = wp_get_theme()->Version;

    // smooth scroll script
    wp_register_script( 'smooth-scroll', get_stylesheet_directory_uri() . '/library/js/libs/jquery.smooth-scroll.js', array( 'jquery' ), '1.4.11' );

    // skrollr stylesheets
    wp_register_script( 'skrollr-stylesheets', get_stylesheet_directory_uri() . '/library/js/libs/skrollr.stylesheets.min.js', array(), '0.0.4', true );

    // skrollr script
    wp_register_script( 'skrollr', get_stylesheet_directory_uri() . '/library/js/libs/skrollr.min.js', array(), '0.6.11' );

    // imagesLoaded script
    wp_register_script( 'images-loaded', get_stylesheet_directory_uri() . '/library/js/libs/imagesloaded.pkgd.min.js', array(), '3.0.4' );

    // unveil script
    wp_register_script( 'unveil', get_stylesheet_directory_uri() . '/library/js/libs/jquery.unveil.min.js', array(), $version );

    // main script
    wp_register_script( 'ktm-js', get_stylesheet_directory_uri() . '/library/js/scripts.js', array( 'smooth-scroll', 'skrollr', 'jquery', 'unveil', 'images-loaded' ), $version );

    // enqueue scripts
    wp_enqueue_script( 'ktm-js' );
    wp_enqueue_script( 'skrollr-stylesheets' );
  }
}
add_action('wp_enqueue_scripts', 'ktm_scripts', 1000);

// Change jpeg quality
add_filter( 'jpeg_quality', 'jpeg_lower_quality' );
function jpeg_lower_quality( $quality ) { return 80; }
?>
