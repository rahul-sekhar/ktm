<?php
/**
 *  Register Field Groups
 *
 *  The register_field_group function accepts 1 array which holds the relevant data to register a field group
 *  You may edit the array as you see fit. However, this may result in errors if the array is not compatible with ACF
 */

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_race-day',
		'title' => 'Race Day',
		'fields' => array (
			array (
				'key' => 'field_52180bc8251d6',
				'label' => 'Schedule',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_52179f4d2e363',
				'label' => 'Schedule Section Visible',
				'name' => 'schedule_visible',
				'type' => 'true_false',
				'message' => '',
				'default_value' => 1,
			),
			array (
				'key' => 'field_52179f932e364',
				'label' => 'Schedule Section Title',
				'name' => 'schedule_title',
				'type' => 'text',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_52179f4d2e363',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => 'Schedule',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52181b2b004dc',
				'label' => 'Dates',
				'name' => 'schedule_dates',
				'type' => 'repeater',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_52179f4d2e363',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'sub_fields' => array (
					array (
						'key' => 'field_52181b56004dd',
						'label' => 'Date',
						'name' => 'date',
						'type' => 'date_picker',
						'column_width' => '',
						'date_format' => 'yymmdd',
						'display_format' => 'DD, d MM yy',
						'first_day' => 1,
					),
					array (
						'key' => 'field_52181bc2004de',
						'label' => 'Schedule',
						'name' => 'schedule',
						'type' => 'wysiwyg',
						'default_value' => '',
						'toolbar' => 'basic',
						'media_upload' => 'no',
					)
				),
				'row_min' => 1,
				'row_limit' => '',
				'layout' => 'table',
				'button_label' => 'Add Date',
			),
			array (
				'key' => 'field_52180bd5251d7',
				'label' => 'Trail',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_52180bec251d8',
				'label' => 'Trail Section Visible',
				'name' => 'trail_visible',
				'type' => 'true_false',
				'message' => '',
				'default_value' => 1,
			),
			array (
				'key' => 'field_52180bf5251d9',
				'label' => 'Trail Section Title',
				'name' => 'trail_title',
				'type' => 'text',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_52180bec251d8',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => 'Trail',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52180bfb251da',
				'label' => 'Trail Section Content',
				'name' => 'trail_content',
				'type' => 'wysiwyg',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_52180bec251d8',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_52180cdd69ae5',
				'label' => 'Transport',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_52180ce669ae6',
				'label' => 'Transport Section Visible',
				'name' => 'transport_visible',
				'type' => 'true_false',
				'message' => '',
				'default_value' => 1,
			),
			array (
				'key' => 'field_52180cfa69ae7',
				'label' => 'Transport Section Title',
				'name' => 'transport_title',
				'type' => 'text',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_52180ce669ae6',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => 'Transport',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52180d1869ae8',
				'label' => 'Transport Section Content',
				'name' => 'transport_content',
				'type' => 'wysiwyg',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_52180ce669ae6',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_52180d2a69ae9',
				'label' => 'Rules',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_52180d3669aea',
				'label' => 'Rules Section Visible',
				'name' => 'rules_visible',
				'type' => 'true_false',
				'message' => '',
				'default_value' => 1,
			),
			array (
				'key' => 'field_52180d4369aeb',
				'label' => 'Rules Section Title',
				'name' => 'rules_title',
				'type' => 'text',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_52180d3669aea',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => 'Rules',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_521810d880522',
				'label' => 'Rules Section Content',
				'name' => 'rules_content',
				'type' => 'wysiwyg',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_52180d3669aea',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_521b05f3e06cf',
				'label' => 'Categories',
				'name' => 'categories',
				'type' => 'repeater',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_52180d3669aea',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'sub_fields' => array (
					array (
						'key' => 'field_521b061de06d0',
						'label' => 'Name',
						'name' => 'name',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_521b0625e06d1',
						'label' => 'Description',
						'name' => 'description',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'br',
					),
				),
				'row_min' => 1,
				'row_limit' => '',
				'layout' => 'table',
				'button_label' => 'Add Category',
			),
			array (
				'key' => 'field_52180d6369aec',
				'label' => 'Volunteer',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_52180eb969aed',
				'label' => 'Volunteer Section Visible',
				'name' => 'volunteer_visible',
				'type' => 'true_false',
				'message' => '',
				'default_value' => 1,
			),
			array (
				'key' => 'field_52180ecf69aee',
				'label' => 'Volunteer Section Title',
				'name' => 'volunteer_title',
				'type' => 'text',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_52180eb969aed',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => 'Volunteer',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52180f0169aef',
				'label' => 'Volunteer Section Content',
				'name' => 'volunteer_content',
				'type' => 'wysiwyg',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_52180eb969aed',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_5218227bed63a',
				'label' => 'Volunteer Sign Up Link',
				'name' => 'volunteer_link',
				'type' => 'text',
				'instructions' => 'Enter the URL for the \'Sign Up\' button. If this is blank, the button will not be shown',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_52180eb969aed',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'race-day.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
?>