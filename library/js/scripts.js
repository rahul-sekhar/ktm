jQuery(document).ready(function ($) {
	if ($('html').hasClass('lt-ie8')) return;

	function isMobile() {
		var width = Math.max( $(window).width(), window.innerWidth);
		return (width < 768);
	}

	function isSmallMobile() {
		var width = Math.max( $(window).width(), window.innerWidth);
		return (width < 481);
	}

	// Hideable content
	$('.hideable-content').hide();
	$('.hideable-title > span').click(function () {
		$(this).parent().toggleClass('expanded').next('.hideable-content').slideToggle();
	});

	// Mobile navigation
	var navMenu = $('#main-nav > ul');
	var mobileMenuShown = false;
	$('#main-nav .pull').click(function (e) {
		e.preventDefault();
		mobileMenuShown = !mobileMenuShown;
		navMenu.slideToggle();
	});

	$(window).resize(function () {
		if (!isMobile()) {
			navMenu.show();
		} else {
			if (mobileMenuShown) {
				navMenu.show();
			} else {
				navMenu.hide();
			}
		}
	});

	// Set the minimum height of the final section of each page to the page height
	if ($('#side-nav').length && !isMobile()) {
		var windowHeight, navTop, bodyBottomPadding, sectionTopPadding, minHeight, lastSection;

		lastSection = $('#main-content > section:last-child');

		windowHeight = $(window).height();
		navTop = $('#side-nav h2').offset().top;
		bodyBottomPadding = parseInt($('#main-wrapper').css('padding-bottom'), 10);
		sectionTopPadding = parseInt(lastSection.css('padding-top'), 10);

		minHeight = windowHeight - navTop - bodyBottomPadding + sectionTopPadding;

		lastSection.css('min-height', minHeight + 'px');
	}

	// Smooth scroll if we're not on the home page
	if (!$('body').hasClass('home')) {
		$('a').smoothScroll();
	}

	// Home page JS
	if ($('body').hasClass('home')) {
		var blogFeedSection = $('#blog-feed');
		var socialFeedSection = $('#social-feed');
		// Store social feed list
		var socialFeedList = socialFeedSection.find('ul').remove();

		// Initialize skrollr with the testimonials end time
		var testimonialsEnd = $('#testimonials').data('testimonialsEnd');
		var skrollrObj = skrollr.init({
			constants : { testimonials_end: testimonialsEnd }
		});

		// Handle scroll-to links
		$('.scroll').click(function (e) {
			var destination, destinationStart, target, currPos;
			e.preventDefault();

			target = $($(this).attr('href'));
			$.each(target[0].attributes, function (i, attr) {
				var data = attr.name.substring(5);
				var val = attr.value;

				if (data.substring(0, 18) == '_testimonials_end-') {
					data = data.substring(18);
					data = parseInt(data, 10);
					data += parseInt(testimonialsEnd, 10);
				} else {
					data = parseInt(data, 10);
				}

				if ($.type(val) == 'string') {
					if (val.indexOf('top[swing]:0%') != -1) {
						destination = data;
					} else if (val.indexOf('top[swing]:100%') != -1) {
						destinationStart = data;
					}
				}
			});

			currPos = $(document).scrollTop();

			if (destinationStart > currPos) {
				skrollrObj.animateTo(destinationStart, {
					duration: (destinationStart - currPos) / 3,
					done: function () {
						skrollrObj.animateTo(destination, { duration: 500 });
					}
				});
			} else {
				skrollrObj.animateTo(destination, { duration: 500 });
			}
		});

		// Testimonials section
		$('#testimonials').on('click', 'a.more', function (e) {
			e.preventDefault();
			$(this).closest('li').find('.full').fadeIn();
		}).on('click', 'a.close', function (e) {
			e.preventDefault();
			$(this).closest('.full').fadeOut();
		}).on('click', '.next', function () {
			// Scroll to next testimonial
			var targetQuote, targetPos

			currPos = $(document).scrollTop();

			$('#testimonials li').each(function () {
				var quoteStart;

				$.each($(this).data(), function (data, val) {
					if (val.indexOf('left[swing]:0%') != -1) {
						 quoteStart = parseInt(data, 10);
						return false
					}
				});

				if ((quoteStart) > currPos) {
					targetQuote = $(this);
					targetPos = quoteStart;
					return false;
				}
			});

			if (targetQuote) {
				skrollrObj.animateTo(targetPos, { duration: 300 });
			} else {
				$('#testimonials + section .scroll').click();
			}
		});
	}

	// Map section
	$('.map-small').click(function() {
		$('#map').fadeIn();
		resizeMap(getNavHeight());
	});

	$('#map .close').click(function(e) {
		e.preventDefault();
		$('#map').fadeOut();
	});

	$('#map .options').on('click', 'a', function(e) {
		e.preventDefault();
		var routeClass = $(this).data('route-class');
		$('#map .route:visible:not(.' + routeClass + ')').fadeOut();
		$('#map .route.' + routeClass).fadeIn();
		$('#map .options li').removeClass('current');
		$(this).closest('li').addClass('current');
	});

	// Gallery Page
	if ($('#main-wrapper').hasClass('gallery')) {

		/* Bib number search */
		var bibSearch = $('li.bib-number');
		bibSearchLink = bibSearch.find('a');
		bibSearchInput = bibSearch.find('input');
		bibSearchForm = bibSearch.find('form');

		function hideBibSearchIfEmpty() {
			if (!bibSearchInput.val()) {
				bibSearchForm.hide();
				bibSearchLink.show();
			}
		}

		// Hide search box initially unless a search has been made
		hideBibSearchIfEmpty();

		// Show search box when the show link is clicked
		bibSearchLink.click(function (e) {
			e.preventDefault();
			bibSearchForm.show();
			bibSearchLink.hide();
			bibSearchInput.focus();
		});

		// Hide search box when the input loses focus
		bibSearchInput.blur(hideBibSearchIfEmpty);

		// Unveil images
		var imagesSection = $('#images');
		imagesSection.find('img').unveil(200, function () {
			var image = this;
			imagesLoaded(image, function (imgs) {
				$(image).parent('.thumb').addClass('loaded');
			});
		});

		/* Show full size images */

		var imageViewer = $('<div id="image-viewer"></div>').appendTo($('#main-wrapper'));
		imageViewer.hide();

		var currentImage = null;

		function closeImageViewer() {
			imageViewer.fadeOut();
		}

		function nextImage() {
			var next = currentImage.next('.thumb');
			if (next.length) {
				loadImage(next);
			} else {
				var nextLink = $('#pagination .next');
				if (nextLink.length) {
					window.location.href = nextLink.attr('href') + '&view_first=1';
				}
			}
		}

		function prevImage() {
			var prev = currentImage.prev('.thumb');
			if (prev.length) {
				loadImage(prev);
			} else {
				var prevLink = $('#pagination .prev');
				if (prevLink.length) {
					window.location.href = prevLink.attr('href') + '&view_last=1';
				}
			}
		}

		// Keybindings
		$('body').on('keydown', function(e) {
			if (imageViewer.is(':visible')) {
				if (e.keyCode == 37) { // Left arrow
					prevImage();
				} else if (e.keyCode == 39) { // Right arrow
					nextImage();
				} else if (e.keyCode == 27) { // ESC
					closeImageViewer();
				}
			}
		});

		// Close link
		$('<a href="#" class="icon-cancel"></a>').click(function (e) {
			e.preventDefault();
			closeImageViewer();
		}).appendTo(imageViewer)

		// Next link
		$('<a href="#" class="icon-right-open-big"></a>').click(function (e) {
			e.preventDefault();
			nextImage();
		}).appendTo(imageViewer)

		// Previous link
		$('<a href="#" class="icon-left-open-big"></a>').click(function (e) {
			e.preventDefault();
			prevImage();
		}).appendTo(imageViewer)

		function loadImage(thumb) {
			var url, screenWidth, widths = [600, 1024, 1600];

			currentImage = thumb;
			imageViewer.find('img').remove();

			screenWidth = Math.max( $(window).width(), window.innerWidth );
			$.each(widths, function (i, width) {
				if (width >= screenWidth) {
					url = currentImage.data('w' + width);
					return false;
				}
			});

			if (!url) {
				url = currentImage.attr('href');
			}
			imageViewer.append('<img src="' + url + '" alt="" />');

			resizeImageViewer();
			imageViewer.fadeIn();
		}

		function resizeImageViewer() {
			var navHeight;
			if (isMobile()) {
				navHeight = 0;
			} else {
				navHeight = $('#main-nav').height();
			}

			imageViewer.height($(window).height() - navHeight);

			var img = imageViewer.find('img');
			var width = img.attr('width');
			var height = img.attr('height');
			var viewerWidth = imageViewer.width();
			var viewerHeight = imageViewer.height();

			if (width / height > viewerWidth / viewerHeight) {
				img.width(viewerWidth);
				img.height(viewerWidth * (height / width));
			} else {
				img.height(viewerHeight);
				img.width(viewerHeight * (width / height));
			}

			if (img.height() < imageViewer.height()) {
				img.css('margin-top', parseInt((imageViewer.height() - img.height()) / 2, 10) + 'px');
			} else {
				img.css('margin-top', '0');
			}
		}

		function resizeGallery() {
			var maxWidth = 300;
			var margin = 3;

			var galleryWidth = imagesSection.width();
			var numImages = Math.ceil(galleryWidth / (maxWidth + margin * 2));
			var imageWidth = Math.floor(galleryWidth / numImages) - (margin * 2);

			imagesSection.find('.thumb').width(imageWidth).height(imageWidth);
		}

		$(window).resize(function () {
			resizeGallery();
			resizeImageViewer(getNavHeight());
		});
		resizeGallery();
		resizeImageViewer(getNavHeight());

		$('#images').on('click', 'a.thumb', function (e) {
			e.preventDefault();
			loadImage($(this));
		});

		// Show image viewer if required
		if (location.search.indexOf('view_first=1') != -1) {
			loadImage($('#images a.thumb:first'));
		} else if (location.search.indexOf('view_last=1') != -1) {
			loadImage($('#images a.thumb:last'));
		}
	}

	// Run resize functions initially and on browser resize
	resizeAll();
	$(window).resize(resizeAll);


	// Records section
	var records = $('.records');
	var tableContainer = records.find('.table-container');

	function switchRecord(target) {
		var source = records.find('table:visible');
		if (target[0] === source[0]) {
			return;
		}

		source.addClass('exiting').fadeOut(function () {
			source.removeClass('exiting');
		});
		target.fadeIn();
	}

	var courseLi = records.find('.course');
	var yearLi = records.find('.year');
	var yearList = yearLi.find('ul');
	var yearLink = records.find('.year > span');

	courseLi.find('span').click(function () {
		yearList.hide();
		yearLink.text('Winners of');
		yearLi.removeClass('current');
		courseLi.addClass('current');
		switchRecord(records.find('table.course'));
	})

	yearLink.click(function (e) {
		e.preventDefault();
		yearList.slideToggle('fast');
	})

	yearList.on('click', 'span', function () {
		yearList.hide();
		yearLink.text('Winners of ' + $(this).text());
		courseLi.removeClass('current');
		yearLi.addClass('current');
		switchRecord(records.find('table.year' + $.trim($(this).text())));
	});

	/* ------------- RESIZING FUNCTIONS ------------ */

	// Calls the various resize functions when the home page is resized
	function getNavHeight() {
		if (isMobile()) {
			return 0;
		} else {
			return $('#main-nav').height();
		}
	}
	function resizeAll() {
		var navHeight = getNavHeight();

		resizeSections(navHeight);
		resizeMap(navHeight);

		if ($('body').hasClass('home')) {
			resizeSocialFeed(navHeight);
			resizeBlogFeed(navHeight);
		}

		justifyNav();
	}

	// Resizes each section to the window height
	function resizeSections(navHeight) {
		var windowHeight;

		windowHeight = $(window).height();
		$('body.home > section, #trail #map').height(windowHeight - navHeight);
	}

	// Resizes the feeds list to fit the screen height in two columns
	function resizeSocialFeed(navHeight) {
		// Remove any lists currently in the feeds section
		socialFeedSection.find('ul').remove();

		// clone the stored social feed list to create the first list
		var firstList = socialFeedList.clone().addClass('first');
		socialFeedSection.append(firstList);

		// Leave the list alone if we're on a small mobile
		if (isSmallMobile()) {
			socialFeedSection.height('auto');
			return;
		}

		// Set the section height
		var sectionMargin = parseInt($('#feeds .content').css('padding-top'), 10);
		socialFeedSection.height($(window).height() - navHeight - (sectionMargin * 2) );

		// find the first overflowing list item
		var sectionBottom = socialFeedSection.offset().top + socialFeedSection.outerHeight();
		var overlappingLi = getElementOverlapping(firstList.find('li'), sectionBottom);

		// If no items are overflowing, exit the function
		if (!overlappingLi) return;

		// If not on small device create a second list
		if (!isMobile()) {
			var secondList = $('<ul class="second"></ul>').appendTo(socialFeedSection);
			overlappingLi.nextAll().andSelf().appendTo(secondList);

			// Remove any overflowing list items from the second list
			overlappingLi = getElementOverlapping(secondList.find('li'), sectionBottom);
		}

		// Remove overlapping elements
		if (overlappingLi) {
			overlappingLi.nextAll().andSelf().remove();
		}
	}

	function resizeBlogFeed(navHeight) {
		// Reset the list classes
		blogFeedSection.find('li').removeClass('hidden');

		// Leave the list alone if we're on a small mobile
		if (isSmallMobile()) {
			blogFeedSection.height('auto');
			return;
		}

		// Set the section height
		var sectionMargin = parseInt($('#feeds .content').css('padding-top'), 10);
		blogFeedSection.height($(window).height() - navHeight - (sectionMargin * 2));

		// hide overflowing list items
		var sectionBottom = blogFeedSection.offset().top + blogFeedSection.outerHeight();
		var overlappingLi = getElementOverlapping(blogFeedSection.find('li'), sectionBottom);

		if (overlappingLi) {
			overlappingLi.nextAll().andSelf().addClass('hidden');
		}
	}

	// Function to find the first element that overlaps a vertical offset position
	function getElementOverlapping(elements, offset) {
		result = null;

		elements.each(function () {
			var current = $(this);
			if((current.offset().top + current.outerHeight()) > offset) {
				result = current;
				return false;
			}
		});

		return result;
	}

	// Resizes the map
	function resizeMap(navHeight) {
		var mapInner = $('#map .inner');
		var responsiveMap = true;

		if (responsiveMap) {
			var currWidth = mapInner.data('imgWidth');
			var newWidth = 'full';
			var widths = [600, 1024, 1600];

			screenWidth = Math.max( $(window).width(), window.innerWidth );
			$.each(widths, function (i, width) {
				if (width >= screenWidth) {
					newWidth = width;
					return false;
				}
			});

			if (newWidth != currWidth) {
				mapInner.data('imgWidth', newWidth);

				mapInner.find('.picture').each(function () {
					var picture = $(this);
					picture.find('img').remove();
					picture.append('<img src="' + picture.data('w' + newWidth) + '" alt="" />');
				});
			}
		}

		var screenHeight = $(window).height() - navHeight;
		var mapHeight = mapInner.width() / mapInner.data('ratio');

		if (mapHeight > 0) {
			mapInner.css('margin-top', parseInt((screenHeight - mapHeight) / 2, 10) + 'px');
		}
	}

	function justifyNav() {
		var navList = $('#main-nav > ul');
		var items = navList.children('li:not(.home-link)');

		if (isMobile()) {
			items.css('margin-right', 0);
			return;
		}

		var width = navList.width();
		var itemsWidth = 0;
		items.each(function () {
			itemsWidth += $(this).innerWidth();
		});
		var extraWidth = width - itemsWidth;
		var itemMargin = Math.floor(extraWidth / (items.length - 1));
		items.css('margin-right', itemMargin + 'px')
			.find('ul').css('right', -1 * itemMargin + 'px');
		items.last().css('margin-right', 0)
			.find('ul').css('right', 0);
	}

	waitForWebfonts(['"Whitney SSm A"'], function () {
		setTimeout(justifyNav, 100);
		setTimeout(justifyNav, 500);
	});
});


function waitForWebfonts(fonts, callback) {
  var loadedFonts = 0;
  for(var i = 0, l = fonts.length; i < l; ++i) {
    (function(font) {
      var node = document.createElement('span');
      // Characters that vary significantly among different fonts
      node.innerHTML = 'giItT1WQy@!-/#';
      // Visible - so we can measure it - but not on the screen
      node.style.position      = 'absolute';
      node.style.left          = '-10000px';
      node.style.top           = '-10000px';
      // Large font size makes even subtle changes obvious
      node.style.fontSize      = '300px';
      // Reset any font properties
      node.style.fontFamily    = 'sans-serif';
      node.style.fontVariant   = 'normal';
      node.style.fontStyle     = 'normal';
      node.style.fontWeight    = 'normal';
      node.style.letterSpacing = '0';
      document.body.appendChild(node);

      // Remember width with no applied web font
      var width = node.offsetWidth;

      node.style.fontFamily = font;

      var interval;
      function checkFont() {
        // Compare current width with original width
        if(node && node.offsetWidth != width) {
          ++loadedFonts;
          node.parentNode.removeChild(node);
          node = null;
        }

        // If all fonts have been loaded
        if(loadedFonts >= fonts.length) {
          if(interval) {
            clearInterval(interval);
          }
          if(loadedFonts == fonts.length) {
            callback();
            return true;
          }
        }
      };

      if(!checkFont()) {
        interval = setInterval(checkFont, 50);
      }
		})(fonts[i]);
  }
};

// Handle external links
jQuery(function ($) {
  $('a').filter(function() {
     return this.hostname && this.hostname !== location.hostname;
  }).attr('target', '_blank');
});
