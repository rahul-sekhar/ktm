<?php
/*
Template Name: Home page
*/
?>

<?php
get_header('home');
?>

<nav id="main-nav" role="navigation">
  <?php
  $ktm_main_nav = KTM_Main_Nav::Instance();
  $ktm_main_nav->display('home');
  ?>

  <a href="#" class="pull">Menu</a>

  <div class="sub-menu">
    <div class="bar"></div>
  </div>
</nav>

<?php
if (have_posts()) : while (have_posts()) : the_post();
?>

<section id="start">
	<h1>
		<?php bloginfo('name'); ?>
	</h1>

	<div class="navbar-cover"></div>
</section>

<section id="brief">
	<div class="content">
    <div class="spacer"></div>

    <div class="dates">
      <div class="inner">
		    <?php the_field('dates'); ?>
      </div>
    </div>

		<div class="info">
      <div class="inner">
			 <?php the_field('text_below_dates'); ?>
      </div>
		</div>

    <div class="spacer"></div>
	</div>

	<a class="scroll" href="#brief">
		<p>Scroll</p>
		<i class="icon-down-open-big"></i>
	</a>
</section>

<?php
$map_color = get_field('map_background_color', 'options');
?>
<section id="map" style="background-color: <?php echo $map_color; ?>;">

  <?php get_template_part('partials/map-inner'); ?>

  <a class="scroll" href="#map">
    <p>Scroll</p>
    <i class="icon-down-open-big"></i>
  </a>
</section>

<?php
$testimonials = to_array(get_field('testimonials'));

$quote_start_time = 1610;

$quote_enter_time = 150;
$quote_stay_time = 250;
$quote_total_time = $quote_enter_time + $quote_stay_time;
$num_quotes = count($testimonials);

$testimonials_exit_time = $quote_start_time - $quote_enter_time + $num_quotes * $quote_total_time;
?>
<section id="testimonials" data-testimonials-end="<?php echo $testimonials_exit_time ?>">
  <div class="next first"><i class="icon-right-open-big"></i></div>

  <ul>
  <?php
  foreach($testimonials as $index => $testimonial) {
  ?>
    <li class="quote-<?php echo $index + 1 ?>">
      <div class="short">
        <?php echo $testimonial['short_quote']; ?>
        <a class="more" href="#">Read more</a>
      </div>

      <div class="profile">

        <img src="<?php echo $testimonial['image']['sizes']['testimonial-image']; ?>" alt="" />
        <p class="name"><?php echo $testimonial['name']; ?></p>
        <div class="description"><?php echo $testimonial['description'] ?></div>
      </div>

      <?php if($index + 1 < count($testimonials)) { ?>
        <div class="next"><i class="icon-right-open-big"></i></div>
      <?php } ?>

      <div class="full">
        <div class="quote">
          <?php echo $testimonial['full_quote']; ?>
        </div>

        <a class="close icon-cancel" href="#"></a>
      </div>
    </li>s
  <?php
  }
  ?>
	</ul>

	<div class="image"></div>

  <a class="scroll" href="#testimonials">
    <p>Scroll</p>
    <i class="icon-down-open-big"></i>
  </a>
</section>

<section id="feeds">
	<div class="content">
		<div id="blog-feed">
			<h2>From our blog</h2>
			<?php ktm_display_recent_posts() ?>
		</div>

		<div id="social-feed">
			<h2>News from Runners for Life</h2>
			<?php ktm_display_feed() ?>
		</div>
	</div>

  <a class="scroll" href="#feeds">
    <p>Scroll</p>
    <i class="icon-down-open-big"></i>
  </a>
</section>


<section id="credits">
  <?php
  $logoColor = 'E2DEA6';
  $logoArea = 8000;
  ?>
  <div class="content">
    <div>
      <div>
        <?php
        for($i = 1; $i <= 2; $i++) {
        ?>
          <div class="sponsors sponsors-<?php echo $i; ?>">
            <p><?php the_field('sponsors_title_' . $i); ?></p>

            <div class="logos">
            <?php
            $logos = get_field('sponsors_logos_' . $i);
            foreach(to_array($logos) as $logo) {
            ?>
              <a href="<?php echo $logo['link']; ?>" title="<?php echo $logo['name']; ?>" target="_blank">
                <img src="<?php echo get_logo_image(get_attached_file($logo['image']['id']), $logoArea, $logoColor) ?>" alt="<?php echo $logo['name']; ?>" />
              </a>
            <?php
            }
            ?>
            </div>
          </div>
        <?php
        }
        ?>
      </div>
    </div>
  </div>

  <a class="scroll" href="#credits">
    <p>Scroll</p>
    <i class="icon-down-open-big"></i>
  </a>
</section>

<?php
endwhile; endif;
get_footer();
?>
