set :db_settings, {
  local: {
    username: "...",
    password: "...",
    database: "...",
    host: "localhost",
    charset: "utf8"
  },
  staging: {
    username: "...",
    password: "...",
    database: "...",
    host: "localhost",
    charset: "utf8"
  },
  production: {
    username: "...",
    password: "...",
    database: "...",
    host: "localhost",
    charset: "utf8"
  }
}