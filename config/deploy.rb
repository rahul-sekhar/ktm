load "config/sensitive"

set :stages, %w(production staging)
set :default_stage, "production"
require 'capistrano/ext/multistage'

default_run_options[:pty] = true
set :application, "ktm"
set :repository,  "git@bitbucket.org:rahul-sekhar/ktm.git"
set :scm, :git
set :ssh_options, { forward_agent: true }

set :keep_releases, 5

set :local_site_url, "ktm.dev"
set :local_uploads_path, "../../uploads"

set :wp_cli_command, "~/.wp-cli/bin/wp"

set :port, 22 unless exists?(:port)

after "deploy:restart", "deploy:cleanup"

task :uname do
  run "uname -a"
end


namespace :uploads  do
  desc "Import uploads from the remote server to the local machine"
  task :import_from_remote, :roles => :app do
    system "rsync -rtvuc -e 'ssh -p #{port}' --delete --progress #{user}@#{host_name}:#{uploads_path}/ #{local_uploads_path}/"
  end

  desc "Export uploads from the local machine to the remote server"
  task :export_to_remote, :roles => :app do
    system "rsync -rtvuc -e 'ssh -p #{port}' --delete --progress #{local_uploads_path}/ #{user}@#{host_name}:#{uploads_path}/"
  end
end


namespace :db do
  desc "Import database from the remote server to the local machine"
  task :import_from_remote, :roles => :app do
    run_locally "ssh -p #{port} #{user}@#{host_name} 'mysqldump -u#{db_settings[stage.to_sym][:username]} -p#{db_settings[stage.to_sym][:password]} -h#{db_settings[stage.to_sym][:host]} #{db_settings[stage.to_sym][:database]} -C -c --skip-add-locks' | mysql -u#{db_settings[:local][:username]} -p#{db_settings[:local][:password]} -h#{db_settings[:local][:host]} #{db_settings[:local][:database]}"
    run_locally "#{wp_cli_command} search-replace http://#{site_url} http://#{local_site_url}"
    run_locally "#{wp_cli_command} search-replace #{db_settings[stage.to_sym][:email]} #{db_settings[:local][:email]}"
  end

  desc "Export database from the local machine to the remote server"
  task :export_to_remote, :roles => :app do
    run_locally "mysqldump -u#{db_settings[:local][:username]} -p#{db_settings[:local][:password]} -h#{db_settings[:local][:host]} -C -c --skip-add-locks #{db_settings[:local][:database]} | ssh -p #{port} #{user}@#{host_name} 'mysql -u #{db_settings[stage.to_sym][:username]} -p#{db_settings[stage.to_sym][:password]} -h#{db_settings[stage.to_sym][:host]} #{db_settings[stage.to_sym][:database]}'"
    run "cd #{current_path} && #{wp_cli_command} search-replace http://#{local_site_url} http://#{site_url}"
    run "cd #{current_path} && #{wp_cli_command} search-replace #{db_settings[:local][:email]} #{db_settings[stage.to_sym][:email]}"
  end
end


# Cache
namespace :cache do
  desc "Flush the cache"
  task :flush, :roles => :app do
    run "cd #{current_path} && #{wp_cli_command} w3-total-cache flush #{wp_cli_command} w3-total-cache flush minify && #{wp_cli_command} w3-total-cache flush database && #{wp_cli_command} w3-total-cache flush object"
  end
end
after "deploy", "cache:flush"

# WP-CLI test
namespace :wp do
  desc "Get wp-cli info"
  task :info, :roles => :app do
    run "cd #{current_path} && #{wp_cli_command} --info"
  end
end

# Media
namespace :media do
  desc "Regenerate images"
  task :regenerate, :roles => :app do
    run "cd #{current_path} && #{wp_cli_command} media regenerate" do |channel, stream, data|
      puts data
      channel.send_data("y\n") if data.include? "y/n"
    end
  end
end


# Shared assets
set :shared_asset_paths, []
set :symlink_only_paths, ['cache', 'library/sensitive']

namespace :shared_assets  do
  namespace :symlinks do
    desc "Setup application symlinks for shared assets"
    task :setup, :roles => [:app, :web] do
      shared_asset_paths.concat(symlink_only_paths).each { |link| run "mkdir -p #{shared_path}/#{link}" }
    end

    desc "Link assets for current deploy to the shared location"
    task :update, :roles => [:app, :web] do
      shared_asset_paths.concat(symlink_only_paths).each { |link| run "ln -nfs #{shared_path}/#{link} #{release_path}/#{link}" }
    end
  end

  desc "Import shared objects from the remote server to the local machine"
  task :import_from_remote, :roles => :app do
    shared_asset_paths.each do |link|
      system "rsync -rP #{user}@#{host_name}:#{shared_path}/#{link} #{link}/.."
    end
  end

  desc "Export shared objects from the local machine to the remote server"
  task :export_to_remote, :roles => :app do
    shared_asset_paths.each do |link|
      system "rsync -rP #{link} #{user}@#{host_name}:#{shared_path}/#{link}/.."
    end
  end
end

before "deploy:setup", "shared_assets:symlinks:setup"
before "deploy:create_symlink", "shared_assets:symlinks:update"