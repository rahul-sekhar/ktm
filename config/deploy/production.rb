set :user, "rfl"
set :use_sudo, false
set :host_name, "direct.kaveritrailmarathon.com"
set :site_url, "kaveritrailmarathon.com"
set :port, 2222
set :deploy_to, "/home2/#{user}/public_html/event_websites/kaveritrailmarathon-2013/deploys/ktm"
set :uploads_path, "/home2/#{user}/public_html/event_websites/kaveritrailmarathon-2013/wp-content/uploads"

server host_name, :app, :web, :db, :primary => true