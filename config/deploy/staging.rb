set :user, "rfl_ktm"
set :use_sudo, false
set :host_name, "ktm.kairi.in"
set :site_url, host_name
set :deploy_to, "/home/#{user}/ktm.kairi.in/deploys/ktm"
set :uploads_path, "/home/#{user}/ktm.kairi.in/wp-content/uploads"

server host_name, :app, :web, :db, :primary => true