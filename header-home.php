<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

  <head>
    <meta charset="utf-8">

    <title><?php ktm_title(); ?></title>

    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">

    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <link rel="stylesheet" type="text/css" href="//cloud.typography.com/6778852/710222/css/fonts.css" />

    <!-- skrollr control stylesheet -->
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/library/css/skrollr.css?ver=<?php echo wp_get_theme()->Version; ?>" data-skrollr-stylesheet />

    <!-- wordpress head functions -->
    <?php wp_head(); ?>
    <!-- end of wordpress head -->

    <?php
    $ktm_main_nav = KTM_Main_Nav::Instance();
    ?>
    <style>
      #main-nav:hover .sub-menu{ height: <?php echo ($ktm_main_nav->get_max_height() * 28) + 15 + 35 ?>px; }

      <?php
      if (have_posts()) : while (have_posts()) : the_post();

        $widths = array(1600, 1024, 600);
        $heights = array(768);
        $start_image = get_field('start_image');
        $testimonials_image = get_field('testimonials_image');

        if ($start_image) { ?>
          #start { background-image: url('<?php echo $start_image['url']; ?>'); }
        <?php } if ($testimonials_image) { ?>
          #testimonials .image { background-image: url('<?php echo $testimonials_image['url']; ?>'); }
        <?php }
          foreach ($widths as $width) {
          ?>
            @media all and (orientation: landscape) and (max-width: <?php echo $width; ?>px) {
            <?php if ($start_image) { ?>
              #start { background-image: url('<?php echo $start_image['sizes']['w' . $width]; ?>'); }
            <?php } if ($testimonials_image) { ?>
              #testimonials .image { background-image: url('<?php echo $testimonials_image['sizes']['w' . $width]; ?>'); }
            <?php } ?>
            }
          <?php
          }

          foreach ($heights as $height) {
          ?>
            @media all and (orientation: portrait) and (max-height: <?php echo $height; ?>px) {
            <?php if ($start_image) { ?>
              #start { background-image: url('<?php echo $start_image['sizes']['h' . $height]; ?>'); }
            <?php } if ($testimonials_image) { ?>
              #testimonials .image { background-image: url('<?php echo $testimonials_image['sizes']['h' . $height]; ?>'); }
            <?php } ?>
            }
          <?php
          }
      endwhile; endif;
      rewind_posts();
      ?>
    </style>
  </head>

  <body <?php body_class(); ?>>
