<?php
/*
Template Name: Page with sections
*/
?>

<?php
get_header();

if (have_posts()) : while (have_posts()) : the_post();
?>

<nav id="main-nav" role="navigation">
  <?php
  $ktm_main_nav = KTM_Main_Nav::Instance();
  $ktm_main_nav->display(sanitize_title(get_the_title()));
  ?>

  <a href="#" class="pull">Menu</a>

  <div class="sub-menu">
    <div class="bar"></div>
  </div>
</nav>

<?php get_template_part( 'partials/top-bar' ); ?>

<div id="main-wrapper" class="content">
  <?php
    get_template_part( 'partials/side-nav' );
  ?>
  <div id="main-content">

    <?php
    foreach(get_field('sections') as $section) {
      if ($section['visible']) {
        echo '<section id="' . sanitize_title($section['title']) . '">';
        echo '<header><h3>' . $section['title'] . '</h3></header>';
        echo $section['content'];
        echo '</section>';
      }
    }
    ?>

  </div>
</div>

<?php
endwhile; endif;

get_footer();
?>