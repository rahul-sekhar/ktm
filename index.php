<?php
/* This page shows blog posts */
?>

<?php
get_header('blog');
?>

<nav id="main-nav" role="navigation">
  <?php
  $ktm_main_nav = KTM_Main_Nav::Instance();
  $posts_page_id = (int) get_option('page_for_posts');
  $ktm_main_nav->display(sanitize_title(get_the_title($posts_page_id)));
  ?>

  <a href="#" class="pull">Menu</a>

  <div class="sub-menu">
    <div class="bar"></div>
  </div>
</nav>

<?php get_template_part( 'partials/top-bar' ); ?>

<div id="main-wrapper" class="content blog">
  <?php
  get_template_part( 'partials/blog-side-nav' );
  ?>
  <div id="main-content">
    <?php

    if (have_posts()) :
      while (have_posts()) : the_post();
        get_template_part( 'partials/post' );
      endwhile;

      ktm_post_nav();

    else:
      get_template_part( 'partials/not-found' );
    endif;
    ?>
  </div>
</div>

<?php get_footer(); ?>
