<section class="not-found">
  <?php if(is_home() || is_archive()) { ?>
    <p>No posts were found.</p>
  <?php } else { ?>
  <header>
    <h3>Sorry!</h3>
  </header>

  <p>We could not find the page you were looking for.</p>
  <?php } ?>
</section>