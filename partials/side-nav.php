<nav id="side-nav">
	<ul>
		<li><h2><?php the_title(); ?></h2></li>
	<?php
	$ktm_main_nav = KTM_Main_Nav::Instance();
	$sub_items = $ktm_main_nav->get_subitems(get_the_title());
	$lazy_output = '';
	foreach ($sub_items as $item) {
		if (isset($item['title'])) {
	?>
		<li><a href="<?php echo $item['url'] ?>"><?php echo $item['title'] ?></a></li>
	<?php
		} elseif (isset($item['function'])) {
			$lazy_output .= '<li' . (isset($item['class']) ? ' class="' . $item['class'] . '"' : '') . '>' . call_user_func($item['function']) . '</li>';
		}
	}

	echo $lazy_output;
	?>
	</ul>
</nav>